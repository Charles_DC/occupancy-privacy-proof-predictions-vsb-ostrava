# New approach for occupancy monitoring
Following the  review rework, an article was found and the idea of 2D sensor reading re-emerged.

    Yang, C.-L.; Chen, Z.-X.; Yang, C.-Y. Sensor Classification Using Convolutional Neural Network by Encoding Multivariate Time Series as Two-Dimensional Colored Images. Sensors 2020, 20, 168. https://doi.org/10.3390/s20010168 

In this folder, the Yang, C.-L.; Chen, Z.-X.; Yang, C.-Y 's methods will be used. This work can be used to compare it with other results from differents methods.