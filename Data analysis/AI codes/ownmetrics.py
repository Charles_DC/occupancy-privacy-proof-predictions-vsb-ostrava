from sklearn.metrics import mean_absolute_error
from sklearn.ensemble import RandomForestRegressor

def get_mae(model, train_X, val_X, train_y, val_y):
    model.fit(train_X, train_y)
    preds_val = model.predict(val_X)
    mae = mean_absolute_error(val_y, preds_val)
    return(mae)

def get_mae_mean_percent(model, train_X, val_X, train_y, val_y, data_mean):
    return 1 - get_mae(model, train_X, val_X, train_y, val_y)/data_mean

def score_model(model, X_t, X_v, y_t, y_v):
    model.fit(X_t, y_t)
    preds = model.predict(X_v)
    return mean_absolute_error(y_v, preds)

# for i in range(0, len(models)):
#     mae = score_model(models[i])
#     print("Model %d MAE: %d" % (i+1, mae))

# Function for comparing different approaches
def score_dataset_on_rf(X_train, X_valid, y_train, y_valid):
    model = RandomForestRegressor(n_estimators=100, random_state=0)
    model.fit(X_train, y_train)
    preds = model.predict(X_valid)
    return mean_absolute_error(y_valid, preds)