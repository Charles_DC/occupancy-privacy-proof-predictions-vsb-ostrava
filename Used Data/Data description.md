# Data description

## Used Data folder
The used data folder is the compilation of all the data used. Thoses who have been formated use the formaat below :

## (WHb) Data format, Wood House based
This represent the data format used for the WH model.
As a multiple data input sources, and only few presence data, this kind of formating is used to train a model, and then afterwards used for different datasets.


|Datetime|S1 ppm|S1 dppm|S1 °C|S1 rH|S2 ppm|S2 dppm|S2 °C|S2 rH|S3 ppm|S3 dppm|S3 °C|S3 rH|
|---|---|---|---|---|---|---|---|---|---|---|---|---|
|ISO-8601 YYYY.MM.DD HH:M|sensor reading|numpy gradient|sensor reading|sensor reading|sensor reading|numpy gradient|sensor reading|sensor reading|sensor reading|numpy gradient|sensor reading|sensor reading|
+ S[1-3] is the sensor name.
+ ppm is the C02 concentration unit
+ dppm is the gradient caluated with numpy
+ °C is the temperature unit
+ rH is the relative humidity in %

