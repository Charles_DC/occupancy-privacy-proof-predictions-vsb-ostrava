from threading import Thread, Semaphore
from time import time, sleep,gmtime,strftime
import pandas as pd
import os

nb_gens = 1
T_sampling = 60 # s
window = 0
store=0
app = True
curr_time_sampling = 0

sem = Semaphore(1)

cols = [ "Datetime", "people number", "window status", "store status"]
data = []

def render ():
    global nb_gens
    global T_sampling
    global window
    global store
    global app
    global curr_time_sampling

    print("\x1b[2J\x1b[H", end='')

    print("People :", nb_gens)
    print("Sampling period (s) :", T_sampling)
    print("Window :", "Opened" if window == 1 else "Closed")
    print("Store :", "Opened" if store == 1 else "Closed")
    print("Samples: ", len(data))
    print("Next sample in :", int(T_sampling - curr_time_sampling), "s")

    print()

    print("Commands :")
    print(" + : Add one people in the room")
    print(" - : Remove one people from the room")
    print(" w : Toggle window state")
    print(" s : Toggle store state")
    print(" q : Quit")

    print()
    print("> ", end="")


def sampling_thread ():
    global nb_gens
    global T_sampling
    global window
    global store
    global app
    global curr_time_sampling
    
    before = time() - T_sampling

    while True:

        after = time()
        dt = after - before

        if dt < T_sampling:
            curr_time_sampling = dt
            render()
            sleep(20)
            continue

        sem.acquire()
        # Get the current time in seconds since the epoch
        current_time_seconds = time()
        offset_seconds = 2 * 3600  # 2 hours * 3600 seconds/hour
        desired_time_seconds = current_time_seconds + offset_seconds
        desired_time_struct = gmtime(desired_time_seconds)
        formatted_time = strftime("%Y-%m-%d %H:%M", desired_time_struct)

        data.append([formatted_time, nb_gens, window, store])
        sem.release()


        file_path = 'people_count.csv'
        df = pd.DataFrame(data, columns=cols)
        if os.path.isfile(file_path):
            #file already exist
            df.to_csv(file_path, index=False, header=False,mode='a')
        else:
            #file isn't created
            df.to_csv(file_path, index=False,mode='w')
        data.clear()

        if not app:
            return
        
        render()
        before = time()


T = Thread(daemon=True, target=sampling_thread)
T.start()

while app:


    cmd = input("")

    sem.acquire()

    match cmd:

        case "+":
            nb_gens += 1

        case "-":
            nb_gens -= 1
            nb_gens = max(0, nb_gens)

        case "w":

            if window == 1:
                window = 0
            else:
                window = 1
        
        case "s":

            if store == 1:
                store = 0
            else:
                store = 1

        case "q":
            app = False

    render()
    sem.release()

print("Quitting ...")