# Results Descriptions

In this file, we will talk about the results graphs provided in this folder (/Results).

## Predictions on FH
All the Predictions on FH have been made with a WH model, the accuracy of the WH model used is noted on the image files. Furthermore, The addition of the normalized C02 curves are here to have indications about the tendencies in the occupancy as this data wasn't provided.
Both networks the 'New' and 'Odl' are showing differents results, this is due to the differents training methods, the 'New' is trained to avoid data over-training.