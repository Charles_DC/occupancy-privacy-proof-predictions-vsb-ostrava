# %%
import numpy as np
import pandas as pd

from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.metrics import accuracy_score, f1_score

import matplotlib.pyplot as plt
import seaborn as sns

# %%
# Load the data
data = pd.read_csv('../outputFOURmonths.csv', header=0, index_col='Datetime', parse_dates=['Datetime'])
len(data)

# %%
print(" ".join(f"'{key}'" for key in data.keys()))

# %%
# real values
y_validation_cat = data["Real values"]

# Regression CNN
basic_regression = data["Basic Regression CNN model"]

# Classification models
y_pred_Newmodel_cat = data["Small 1DCNN Classification New model predictions"]

y_pred_CNN_depth_cat = data["Deep 1DCNN Classification model 11th layers"]
y_pred_CNN_depth1024_cat = data['Deep 1DCNN Classification model 11th layers + 1024 neurons  per layers']
y_pred_CNN_depth1024_2500_cat = data['Deep 1DCNN Classification model 11th layers + 1024 neurons  per layers + 2500 epoch']

# %%
#plot figures:
plt.figure(figsize=(15,15))
plt.subplot(4, 1, 1)
plt.plot(y_validation_cat)
plt.title('references values')
plt.xlabel('sample number')
plt.ylabel('Number of people')

plt.subplot(4, 1, 2)
sns.lineplot(data=y_validation_cat, label='references values')
sns.lineplot(data=basic_regression, label='basic_regression predictions',linestyle='--')
plt.title('1DCNN Regressions results')
plt.xlabel('sample number')
plt.ylabel('Number of people')
plt.tight_layout()
plt.legend()

plt.subplot(4, 1, 3)
sns.lineplot(data=y_validation_cat, label='references values')
sns.lineplot(data=y_pred_Newmodel_cat, label='Newmodel predictions',linestyle='--')
sns.lineplot(data=y_pred_CNN_depth_cat, label='CNN_depth predictions',linestyle='--')
plt.title('1DCNN Classification results 1/2')
plt.xlabel('sample number')
plt.ylabel('Number of people')
plt.tight_layout()
plt.legend()

plt.subplot(4, 1, 4)
sns.lineplot(data=y_validation_cat, label='references values')
sns.lineplot(data=y_pred_CNN_depth1024_cat, label='CNN_depth1024 predictions',linestyle='--')
sns.lineplot(data=y_pred_CNN_depth1024_2500_cat, label='CNN_depth1024_2500 predictions',linestyle='--')
plt.title('1DCNN Classification results 2/2')
plt.xlabel('Time')
plt.ylabel('Number of people')
plt.legend()

plt.tight_layout()
# plt.show()

plt.savefig('Four Month results.png')
# %% [markdown]
# # Metrics

# %%
accuracy = accuracy_score(y_validation_cat, y_pred_Newmodel_cat)
f1 = f1_score(y_validation_cat, y_pred_Newmodel_cat, average='macro')

print('Newmodel metrics:')
print(f'Accuracy: {accuracy}')
print(f'F1 Score: {f1}')

# %%
ypred = y_pred_CNN_depth_cat
accuracy = accuracy_score(y_validation_cat, ypred)
f1 = f1_score(y_validation_cat, ypred, average='macro')

print('CNN_depth metrics:')
print(f'Accuracy: {accuracy}')
print(f'F1 Score: {f1}')

# %%
ypred = y_pred_CNN_depth1024_cat
accuracy = accuracy_score(y_validation_cat, ypred)
f1 = f1_score(y_validation_cat, ypred, average='macro')

print('CNN_depth1024 metrics:')
print(f'Accuracy: {accuracy}')
print(f'F1 Score: {f1}')

# %%
ypred = y_pred_CNN_depth1024_2500_cat
accuracy = accuracy_score(y_validation_cat, ypred)
f1 = f1_score(y_validation_cat, ypred, average='macro')

print('CNN_depth1024_2500 metrics:')
print(f'Accuracy: {accuracy}')
print(f'F1 Score: {f1}')

# %%
# Calculate additional metrics
mae = mean_absolute_error(y_validation_cat, basic_regression)
mse = mean_squared_error(y_validation_cat, basic_regression)
rmse = np.sqrt(mse)
r2 = r2_score(y_validation_cat, basic_regression)

print(f'Mean Absolute Error (MAE): {mae}')
print(f'Mean Squared Error (MSE): {mse}')
print(f'Root Mean Squared Error (RMSE): {rmse}')
print(f'R-squared (R2): {r2}')


